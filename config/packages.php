<?php

return [
    'main' => [
        'laravel' => [
            'title' => 'Laravel',
            'package_name' => 'laravel/laravel',
            'commands' => [
                'composer create-project laravel/laravel %s',
                'cd %s',
            ]
        ],
        'laravelzero' => [
            'title' => 'Laravel Zero (CLI)',
            'package_name' => 'laravel-zero/laravel-zero',
            'commands' =>
            [
                'composer create-project --prefer-dist laravel-zero/laravel-zero %s',
                'cd %s',
            ]
        ],
        'laraveljetstream' => [
            'title' => 'Laravel Jetstream ',
            'package_name' => 'laravel-zero/laravel-zero',
            'commands' =>
            [
                'composer create-project laravel/laravel %s',
                'cd %s',
                'composer require laravel/jetstream',
                'php artisan breeze:install',
                'php artisan migrate',
                'npm install',
                'npm run dev',
            ]
        ],
        'laravelbreeze' => [
            'title' => 'Laravel Breeze',
            'package_name' => 'laravel/breeze',
            'dev' => true,
            'commands' => [
                'composer create-project laravel/laravel %s',
                'cd %s',
                'composer require laravel/breeze --dev',
                'php artisan breeze:install',
                'php artisan migrate',
                'npm install',
                'npm run dev',
            ],
        ],
        'laravellivewire' => [
            'title' => 'Laravel Livewire',
            'package_name' => 'laravel/livewire',
            'commands' => [
                'composer create-project laravel/laravel %s',
                'cd %s',
                'composer require livewire/livewire',
            ]
        ],
    ]
];
