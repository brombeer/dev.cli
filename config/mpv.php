<?php

return [
    'channels' => [
        "groovesalad" => "http://ice1.somafm.com/groovesalad-128-mp3",
        "groovesaladclassic" => "http://ice1.somafm.com/gsclassic-128-mp3",
        "dronezone" => "http://ice1.somafm.com/dronezone-128-mp3",
        "spacestation" => "http://ice1.somafm.com/spacestation-128-mp3",
        "missioncontrol" => "http://ice1.somafm.com/missioncontrol-128-mp3",
    ]
];
