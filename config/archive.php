<?php

return [
    'folder' => '_archive',
    'exclude_folders' => [
        'vendor',
        'node_modules',
        '.phpintel',
        '.env'
    ]
];
