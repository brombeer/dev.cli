{
"name": "{{ $name }}",
"description": "{{ $description }}",
"keywords": "{{ $keywords }}",
"authors": {
"name": "{{ $author->name }}",
"homepage": "{{ $author->homepage }}"
}
}
