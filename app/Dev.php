<?php

namespace App;

use function Termwind\{render};
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Foundation\Application;

class Dev
{

    public function __construct(private readonly Application $app)
    {
    }

    public static function _header(string $headertext)
    {
        render('<div class="pt-1 ml-1">
            <div class="px-1 bg-green-300 text-black">' . $headertext . '</div>
        </div>');

        // $this->line(sprintf("%s", self::colorOutput($headertext)));
    }


    public static function appTitle() : string
    {
        $name = config('app.name');
        $version = config('app.version');
        return sprintf('%s %s', $name, $version);
    }


    public static function header(string $header, string $content = null)
    {
        $msg = '';
        $msg .= '<div class="pt-1 ml-1">';
        $msg .= '<div class="px-1 bg-blue-300 text-black">' . $header . '</div>';
        if (isset($content)) {
            $msg .= '<em class="ml-1">' . $content . '</em>';
        }
        $msg .= '</div>';

        render($msg);
    }

    public static function appName()  : string {
        $appName = '';
        $appName .= '<div class="">';
        $appName .= '<span class="font-bold text-yellow">' . config('app.name') . '</span>';
        $appName .= ' ';
        $appName .= '<span class="text-white">';
        $appName .= config('app.version');
        $appName .= '</span>';
        $appName .= '</div>';

        return $appName;
    }

    public static function showAppHeader($message = null)
    {
        $msg = '';
        $msg .= '<div class="">';
        $msg .= '<span class="font-bold text-yellow">' . config('app.name') . '</span>';
        $msg .= ' ';
        $msg .= '<span class="text-white">';
        $msg .= config('app.version');
        $msg .= '</span>';
        $msg .= ': ';
        $msg .= '<span class="text-white">';
        $msg .= $message;
        $msg .= '</span>';
        $msg .= '</div>';

        render($msg);
    }

    public static function startCommand($message = null) {
        self::showAppHeader($message);
        Log::info($message);
    }

    public static function appHeader()
    {
        $msg = sprintf('%s [%s]', config('app.name'), config('app.version'));
        self::header($msg);
    }

    public static function keyValue(string $key, string $value)
    {
        $text = sprintf("<fg=yellow>%s</>: <fg=bright-white>%s</>", $key, $value);
        return $text;
    }
}
