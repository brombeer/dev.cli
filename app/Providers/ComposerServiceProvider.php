<?php

namespace App\Providers;

use App\Commands\Composer\ComposerHelper;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        /*
        Within the register method, you should only bind things into the service container.
           You should never attempt to register any event listeners, routes, or any other piece
           of functionality within the register method.
        */
        $this->app->singleton(ComposerHelper::class);
    }
}
