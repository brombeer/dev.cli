<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        # ensure you configure the right channel you use
        // config(['logging.channels.single.path' => \Phar::running()
        //     ? dirname(\Phar::running(false)) . '/logs/dev.log'
        //     : storage_path('logs/dev.log')
        // ]);

        config(['logging.channels.single.path' => \Phar::running()
            ? dirname(\Phar::running(false)) . '/logs/dev.log'
            : storage_path('logs/dev.log')
        ]);
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }
}
