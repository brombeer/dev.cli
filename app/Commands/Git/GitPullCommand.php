<?php

namespace App\Commands\Git;

use function Laravel\Prompts\info;
use Illuminate\Support\Facades\Log;
use App\Commands\Git\BaseGitCommand;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Support\Facades\Process;
use Illuminate\Console\Scheduling\Schedule;

class GitPullCommand extends BaseGitCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'git:pull';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Git pull changes from a remote repository';

    protected $commands = [
        'php artisan queue:work'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Log::info('Running Laravel queue server');

        if (!file_exists('artisan')) {
            $this->warn('Laravel "artisan" not found');
            exit;
        }
        $this->info('Laravel "artisan" found');

        $this->runProcess($this->commands[0]);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
