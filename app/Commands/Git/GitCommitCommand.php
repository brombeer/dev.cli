<?php

namespace App\Commands\Git;

use function Laravel\Prompts\info;
use Illuminate\Support\Facades\Log;
use App\Commands\Git\BaseGitCommand;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Support\Facades\Process;
use Illuminate\Console\Scheduling\Schedule;

class GitCommitCommand extends BaseGitCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'git:commit';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Git commit files';

    protected $commands = [
        '~/go/bin/MailHog %'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Log::info('Running mail server');

        $this->runProcess($this->commands[0]);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
