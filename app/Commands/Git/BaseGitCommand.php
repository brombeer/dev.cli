<?php

namespace App\Commands\Git;

use App\Commands\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Process\InvokedProcess;
use Illuminate\Support\Facades\Process;

class BaseGitCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'git';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'The base git command';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
