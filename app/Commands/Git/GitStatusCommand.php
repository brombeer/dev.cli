<?php

namespace App\Commands\Git;

use Illuminate\Support\Facades\Log;
use App\Commands\Git\BaseGitCommand;
use Illuminate\Support\Facades\Process;
use Illuminate\Console\Scheduling\Schedule;

class GitStatusCommand extends BaseGitCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'git:status';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Git show various information';

    protected $commands = [
        'npm run dev'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Log::info('Running vite assets server');

        $this->runProcess($this->commands[0]);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
