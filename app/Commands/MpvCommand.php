<?php

namespace App\Commands;

use function Laravel\Prompts\info;
use function Laravel\Prompts\select;
use Illuminate\Support\Facades\Process;
use LaravelZero\Framework\Commands\Command;

class MpvCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'soma {channel? : A somaFM channel name to play (i.e. groove-salad, spacenight)';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Play a somaFM channel using mpv';
    protected $channels;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->channels = config('mpv.channels');
        // Swap keys and values for the options
        $options = array_flip(($this->channels));
        // dd($options);
        $channel = select(
            'Which channel do you want me to play?',
            $options,
        );

        $cmd = sprintf('mpv %s', $channel);
        $process = Process::run($cmd);
        if ($process->successful()) {
            info(sprintf('Folder 213123'));
        }
        // return trim($process->getOutput()) ?: 'unreleased';

    }
}
