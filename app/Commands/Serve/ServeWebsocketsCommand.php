<?php

namespace App\Commands\Serve;

use App\Dev;
use function Laravel\Prompts\info;
use Illuminate\Support\Facades\Log;
use App\Commands\Serve\ServeCommand;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Support\Facades\Process;
use App\Commands\Serve\BaseServeCommand;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class ServeWebsocketsCommand extends BaseServeCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'serve:websockets';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Starts the "soketi" websockets server';

    protected $commands = [
        '/storage/home/iko/.nvm/versions/node/v16.20.0/bin/soketi start'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Dev::startCommand('Running "soketi" websockets server');

        $this->runProcess($this->commands[0]);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
