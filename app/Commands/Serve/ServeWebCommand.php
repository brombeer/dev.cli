<?php

namespace App\Commands\Serve;

use App\Dev;
use function Laravel\Prompts\info;
use Illuminate\Support\Facades\Log;
use App\Commands\Serve\ServeCommand;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Support\Facades\Process;
use App\Commands\Serve\BaseServeCommand;
use Illuminate\Console\Scheduling\Schedule;

class ServeWebCommand extends BaseServeCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'serve:web';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Starts a web server. "php artisan serve" first, 11ty maybe, PHP Server next';

    protected $commands = [];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Dev::startCommand('Running web server');

        if (file_exists('artisan')) {
            // Start a Laravel webserver
            $this->d("Laravel artisan found, running 'php artisan serve'");
            $this->commands[] = [
                'php artisan serve',
            ];
        } elseif (file_exists(('.eleventy.js'))) {
            // Running 11ty
            $this->d(".eleventy.js found, running 11ty server");
            $this->commands[] = [
                'npx @11ty/eleventy --serve',
            ];
        } else {
            // Running PHP server
            $this->d("Running a PHP server");
            if (file_exists('public')) {
                $this->d("'public/' folder found");
                $this->commands[] = [
                    'php -S 127.0.0.1:8000 -t public',
                ];
            } else {
                $this->commands[] = [
                    '/usr/bin/php -S 127.0.0.1:8000',
                ];
            }
        }

        $this->runProcess($this->commands[0]);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
