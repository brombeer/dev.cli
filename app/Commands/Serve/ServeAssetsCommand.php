<?php

namespace App\Commands\Serve;

use App\Dev;
use Illuminate\Support\Facades\Log;
use App\Commands\Serve\ServeCommand;
use Illuminate\Support\Facades\Process;
use App\Commands\Serve\BaseServeCommand;
use Illuminate\Console\Scheduling\Schedule;

class ServeAssetsCommand extends BaseServeCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'serve:assets';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Starts a vite assets server';

    protected $commands = [
        'npm run dev'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Dev::startCommand('Running vite assets server');

        $this->runProcess($this->commands[0]);
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
