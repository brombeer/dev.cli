<?php

namespace App\Commands\Serve;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Symfony\Component\Process\Process;

class ServeHelper {

    // protected App $app;

    public function __construct(private readonly Application $app)
    {
        $this->app = $app;
    }

}
