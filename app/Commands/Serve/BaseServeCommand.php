<?php

namespace App\Commands\Serve;

use App\Commands\Command;
use function Laravel\Prompts\info;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Process\InvokedProcess;
use Illuminate\Support\Facades\Process;

class BaseServeCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'serve';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Serves all available servers using "tilix"';

    protected function runProcess($command) : InvokedProcess {
        $process = Process::timeout(10)->start($command);
        $this->line($process->output());
        while ($process->running()) {
            $out = trim($process->latestOutput());
            if (!empty($out)) {
                $this->line(
                    sprintf(
                        '[%s] Length:%s - %s',
                        date('Y-m-d H:i:s'),
                        strlen(trim($out)),
                        $out
                    )
                );
            }
            sleep(10);
        }
        $result = $process->wait();
        $this->line('Process finished');
        // dump($result);

        return $process;
    }

    /**
     * Summary of d
     * @param string $message
     * @return void
     */
    protected function d(string $message) {
        $this->line($message);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
