<?php

namespace App\Commands\Serve;

use App\Dev;
use function Laravel\Prompts\info;
use Illuminate\Support\Facades\Log;
use App\Commands\Serve\ServeCommand;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Support\Facades\Process;
use App\Commands\Serve\BaseServeCommand;
use Illuminate\Console\Scheduling\Schedule;

class ServeScheduleCommand extends BaseServeCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'serve:schedule';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Starts the Laravel scheduler';

    protected $commands = [
        'php artisan schedule:work'
    ];

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Dev::startCommand('Running Laravel schedule server');

        if (!file_exists('artisan')) {
            $this->warn('Laravel "artisan" not found');
            exit;
        }
        $this->d('Laravel "artisan" found');

        $this->runProcess($this->commands[0]);
    }

    /**
     * D
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
