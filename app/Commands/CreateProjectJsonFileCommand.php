<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class CreateProjectJsonFileCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'create:projectjson
    {maintainer-name?}
    {maintainer-homepage?}
    {project-name?}
    {project-homepage?}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Creates a project.json file';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Ask for project name

        // Ask for project description

        // Ask for project version

        // Ask for/fill in authors name
        // Ask for/fill in authors homepage
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
