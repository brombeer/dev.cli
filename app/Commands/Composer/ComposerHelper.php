<?php

namespace App\Commands\Composer;

use Illuminate\Contracts\Foundation\Application;

class ComposerHelper {

    public function __construct(private readonly Application $app)
    // public function __construct()
    {
    }

    public static function isInstalled() : bool
    {
        return true;
    }

}
