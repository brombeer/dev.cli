<?php

namespace App\Commands\Archive;

use function Termwind\{render};
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Process;

class ArchiveHelper
{

    protected $archiveFolder;
    protected $excludeFolders;

    public function __construct(private readonly Application $app)
    {
    }

    public function checkRequirements(): void
    {
        $this->archiveFolder = config('archive.folder');
        $this->excludeFolders = config('archive.exclude_folders');
        $this->makeSureArchiveFolderExists();
    }

    public function archiveFolder(string $source)
    {
        $foldername = basename($source);
        $target = $this->archiveFolder . '/' . $foldername . '.tgz';
        $excludes = array_map(function ($folder) {
            return sprintf("--exclude='%s'", $folder);
        }, $this->excludeFolders);
        $exclude = implode(' ', $excludes);
        $source = $foldername;
        $cmd = sprintf(' tar cfpz %s %s ./%s', $target, $exclude, $source);
        // $this->info($cmd);
        $result = Process::run($cmd);
        if ($result->successful()) {
            if (File::deleteDirectory($source)) {
                // $this->info(sprintf(' Folder <fg=white>%s</> archived to <fg=white>%s</>', $source, $target));
                return true;
            }
        }
        return false;
    }

    public function description(): string
    {
        $description = sprintf(
            "Pack the given folder into a <fg=bright-white>.tgz</> archive file. Excluding the following files/folders: %s",
            implode(', ', $this->excludeFolders)
        );

        return $description;
    }

    private function makeSureArchiveFolderExists()
    {
        if (!File::isDirectory($this->archiveFolder)) {
            File::makeDirectory($this->archiveFolder);
        }
    }
}
