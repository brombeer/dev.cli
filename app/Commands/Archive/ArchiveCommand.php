<?php

namespace App\Commands\Archive;

use App\Commands\Archive\ArchiveHelper;
use App\Commands\Command;
use function Laravel\Prompts\suggest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class ArchiveCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'archive
    {foldername? : Name of the folder to archive}
    {--simulate : Don\'t create or move files, only show simulation output}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Archive a folder into the "_archive" folder. ("vendors/", "node_modules/" etc. will be skipped)';

    private $foldername;
    private $simulate = true;

    protected $simulationHeader = '<fg=yellow;bg=bright-blue;options=bold>[SIMULATION MODE]</>';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->foldername = $this->argument('foldername');
        $this->simulate = $this->option('simulate');

        if ($this->simulate) {
            $this->info($this->simulationHeader . ' No files will be created or moved!');
        }

        $a = new ArchiveHelper($this->app);
        $a->checkRequirements();

        $this->line($a->description());
        $this->foldername = $this->askForFoldername();
        $folderArchive = config('archive.folder') . '/' . $this->foldername . ".tgz";
        $folderMessage = 'Folder <fg=white>%s</> archived to <fg=white>%s</>';
        $folderMessage = sprintf($folderMessage, $this->foldername, $folderArchive);

        // TODO: duplicate file name check, increment archive name if already exists
        if (!$this->simulate) {
            if ($a->archiveFolder($this->foldername)) {
                $this->info($folderMessage);
            }
        } else {
            $this->info($this->simulationHeader . ' ' . $folderMessage);
        }
        Log::info(strip_tags($folderMessage));

        return false;
    }

    protected function askForFoldername(): string
    {
        if (!is_null($foldername = $this->argument('foldername'))) {
            // return $foldername;
        }
        if (!File::isDirectory(getcwd() . '/' . $foldername)) {
            $this->warn(sprintf('Folder <fg=white>%s</> does not exist', getcwd() . '/' . $foldername . '/'));
            // $this->info('Exiting');
            // exit;
        }

        $foldersInCurrentDirectory = glob('[!_]*', GLOB_ONLYDIR);
        // dd($foldersInCurrentDirectory);
        $foldername = suggest(
            'Which folder do you want to archive?',
            $foldersInCurrentDirectory,
            required: true,
        );

        return $foldername;
    }
}
