<?php

namespace App\Commands\Info;

use App\Commands\Command;
use App\Commands\Info\InfoHelper;
use App\Dev;

class InfoPHPCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'info:php';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Show PHP information';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Dev::header('PHP');
        $this->align([
            'OS' => InfoHelper::getPHPOS(),
            'Version' => InfoHelper::getPHPVersion(),
            'Binary' => InfoHelper::getPHPBinary(),
            'IncludePath' => InfoHelper::getPHPIncludePath(),
            'Extensions' => InfoHelper::getPHPExtensions(),
            'php.ini' => InfoHelper::getPHPIniInformation(),
        ]);
    }
}
