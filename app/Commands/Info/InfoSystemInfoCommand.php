<?php

namespace App\Commands\Info;

use App\Commands\Command;
use App\Commands\Info\Info;
use App\Dev;

class InfoSystemInfoCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'info:system';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Show system information';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        Dev::header('System info');
        $this->align([
            'dev version' => InfoHelper::getAppVersion(),
            'Current path' => InfoHelper::getCwd(),
            'Date & Time' => InfoHelper::getDateTime(),
        ]);
    }
}
