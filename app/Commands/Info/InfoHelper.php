<?php

namespace App\Commands\Info;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Symfony\Component\Process\Process;

class InfoHelper {

    // protected App $app;

    public function __construct(private readonly Application $app)
    {
        $this->app = $app;
    }

    public static function getAppVersion()
    {
        return config('app.version');
    }
    public static function getCwd()
    {
        return getcwd() ?? 'unknown';
    }
    public static function getDateTime()
    {
        return date('d.m.Y, H:i:s');
    }
    public static function getPHPOS()
    {
        return PHP_OS;
    }
    public static function getPHPVersion()
    {
        return PHP_VERSION;
    }
    public static function getPHPBinary()
    {
        return PHP_BINARY;
    }
    public static function getPHPIncludePath()
    {
        return DEFAULT_INCLUDE_PATH;
    }
    public static function getPHPExtensions()
    {
        return implode(',', get_loaded_extensions());
    }

    public static function getPHPIniInformation() {
        $process = Process::fromShellCommandline(
            'php -i | grep php.ini'
        );

        $process->run();
        $data = trim($process->getOutput()) ?: 'nope';

        return $data;


    }
}
