<?php

namespace App\Commands\Info;

use App\Commands\Command;
use Illuminate\Support\Facades\Log;

class InfoCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'info';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Show combined system information';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Log::info('Running system info');

        $this->call('info:system');
        $this->call('info:php');
    }
}
