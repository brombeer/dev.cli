<?php

namespace App\Commands;

use App\Dev;
use function Termwind\{render};
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command as RootCommand;

abstract class Command extends RootCommand
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'app:command';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected static function colorOutput(string $output, string $color = "default")
    {
        return sprintf("<fg=%s>%s</>", $color, $output);
    }

    protected function align(array $data, int $offset = 1)
    {
        $maxlen = max(array_map('strlen', array_keys($data)));
        foreach ($data as $key => $value) {
            $this->line(
                Dev::keyValue(
                    str_pad($key, $maxlen + $offset, " ", STR_PAD_LEFT),
                    $value
                )
            );
        }
    }

    protected function out(array $textSprintf, string $type = 'default')
    {
        $text = $textSprintf[0];
        array_shift($textSprintf);
        $parameters = $textSprintf;
        $text = sprintf($text, ...$parameters);
        if ($type == 'line' || $type == 'default') {
            $this->line($text);
        } elseif ($type == 'warn') {
            $this->warn($text);
        } elseif ($type == 'error') {
            $this->error($text);
        } elseif ($type == 'info') {
            $this->info($text);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
