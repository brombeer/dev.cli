<?php

namespace App\Commands\Clear;

use App\Commands\Command;
use App\Composer;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Process;

class ClearCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'clear';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Clear your projects cached files (config, views, routes...)';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        Log::info('Running clear command');

        $process = Process::fromShellCommandline(
            'git describe --tags --abbrev=0',
            $this->app->basePath()
        );

        $process->run();

        return trim($process->getOutput()) ?: 'unreleased';
        /*
        $this->task("Clearing cache", function () {
            return true;
        });
        $this->task("Re-index vendor dependencies", function () {

            return true;
        });
        echo Composer::isInstalled();
 */
    }
}
