<?php

namespace App\Commands;

use function Termwind\{render};
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class InspireCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'inspire {name=Artisan}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Display an inspiring quote';

    private array $quotes = [
        'Simplicity is the ultimate sophistication.',
        'Exercitation culpa nulla veniam velit consectetur incididunt proident aliquip.',
        'Aliquip non anim esse ad velit dolore enim dolor.',
        'Aute veniam et nulla mollit.',
    ];

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $msg = $this->quotes[rand(0, count($this->quotes)-1)];
        render(<<<HTML
            <div class="py-1 ml-2">
                <div class="px-1 bg-blue-300 text-black">dev</div>
                <em class="ml-1">
                    $msg
                </em>
            </div>
        HTML);
    }

    /**
     * Define the command's schedule.
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
