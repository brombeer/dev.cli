<?php

namespace App\Commands\NewCommand;

use App\Commands\Command;
use function Laravel\Prompts\select;
use function Laravel\Prompts\suggest;
use Illuminate\Console\Scheduling\Schedule;
use Log;

class NewCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    // protected $signature = 'new {projectname? : The name of the new project. A folder with the same name will be created, files will be installed into this folder';
    protected $signature = 'new
                            {name? : The name of the new project}
                            {--default=laravelzero}
                            {--list : List available defaults}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Creates and prepares a new project for you to develop. A folder with the project\'s name will be created, files will be installed into this folder.';
    private $packages_main;
    private $words_start;
    private $words_end;

    private function randomName()
    {
        return $this->words_start->random() . '-' . $this->words_end->random();
    }

    private function askType()
    {
        $default = $this->option('default');
        $options = $this->packages_main->mapWithKeys(function ($item, $key) {
            return [$key => $item['title']];
        });

        $preset = select(
            label: 'What type of project do you want to start?',
            options: $options,
            default: $default,
            scroll: 4,
        );
        return $preset;
    }

    private function askName()
    {
        $suggestions = [
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
            $this->randomName(),
        ];
        $name = suggest(
            'How do you want to name your project?',
            $suggestions,
            required: true,
        );

        return $name;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
        $this->packages_main = collect(config('packages.main'));
        $this->words_start = collect(config('new.words_start'));
        $this->words_end = collect(config('new.words_end'));

        $name = $this->askName();
        // info('You selected ' . $name);

        // check if a folder with that name already exists
        if (file_exists($name)) {
            $this->warn("A folder named <fg=white>$name/</> already exists.");
            exit;
        }

        $type = $this->askType();
        $cmds = '';

        info(sprintf('New %s project "%s" created. Have fun coding!', $type, $name));
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
