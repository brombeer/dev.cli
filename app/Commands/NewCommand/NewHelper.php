<?php

namespace App\Commands\NewCommand;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Symfony\Component\Process\Process;

class NewHelper {

    // protected App $app;

    public function __construct(private readonly Application $app)
    {
        $this->app = $app;
    }

}
